from typing import List
from app.show_car_park import Show
from app.find_player import Player
from app.game import Game


class Escape:
    def __init__(self, car_park: List):
        self.car_park = car_park

    def show_car_park(self):
        return Show(self.car_park).show_car_park()

    def find_player(self):
        return Player(self.car_park).find_player()

    def start(self):
        return Game(self.car_park, self.find_player()).game_escape()

