class StairPlayer:
    def __init__(self, player, stair):
        self.player = player
        self.stair = stair

    def stair_less_than_player(self):
        return True if self.stair < self.player else False
