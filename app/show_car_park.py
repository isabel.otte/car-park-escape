from typing import List


class Show:
    def __init__(self, car_park: List):
        self.car_park = car_park

    def show_car_park(self):
        for i in self.car_park:
            print(i)
