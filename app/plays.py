from typing import List


class Plays:

    def right_plays(self, plays: List, player, stair):
        plays.append('R' + str(stair - player))
        plays.append('D1')
        return plays

    def left_plays(self, plays: List, player, stair):
        plays.append('L' + str(player - stair))
        plays.append('D1')
        return plays

    def last_play(self, plays: List, car_park: List, player):
        plays.append('R' + str((len(car_park[-1]) - 1) - player))
        return plays
