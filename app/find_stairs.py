from typing import List


class Stair:
    def __init__(self, floor: List):
        self.floor = floor

    def find_stair(self):
        return True if 1 in self.floor else False
