from typing import List
from app.find_stairs import Stair
from app.player_less_than_stair import PlayerStair
from app.stair_less_than_player import StairPlayer
from app.player_equal_stair import Equal
from app.sequence import SequenceOfLetter
from app.last_play import LastPlay
from app.plays import Plays


class Game:
    def __init__(self, car_park: List, player):
        self.car_park = car_park
        self.player = player

    def game_escape(self):
        plays = []
        count = 1
        stair = 0
        while True:
            for i in self.car_park:
                if Stair(i).find_stair():
                    stair = list(i).index(1)
                    if PlayerStair(self.player, stair).player_less_than_stair():
                        plays = Plays().right_plays(plays, self.player, stair)
                        count = 1
                    elif StairPlayer(self.player, stair).stair_less_than_player():
                        plays = Plays().left_plays(plays, self.player, stair)
                        count = 1
                    elif Equal(self.player, stair).player_equal_stair():
                        if SequenceOfLetter(plays):
                            count += 1
                            plays[-1] = 'D' + str(count)
                        else:
                            plays.append('D1')
                else:
                    if LastPlay(self.player, self.car_park).check_last_play():
                        plays = Plays().last_play(plays, self.car_park, self.player)

                self.player = stair

            break

        return plays
