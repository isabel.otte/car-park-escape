from typing import List


class LastPlay:
    def __init__(self, player, car_park: List):
        self.player = player
        self.car_park = car_park

    def check_last_play(self):
        return True if self.player != (len(self.car_park[-1]) - 1) else False
