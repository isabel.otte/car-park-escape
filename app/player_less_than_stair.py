class PlayerStair:
    def __init__(self, player, stair):
        self.player = player
        self.stair = stair

    def player_less_than_stair(self):
        return True if self.player < self.stair else False
