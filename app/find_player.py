from typing import List


class Player:
    def __init__(self, car_park: List, player = None):
        self.car_park = car_park
        self.player = 0

    def find_player(self):
        for i in self.car_park:
            if 2 in i:
                self.player = list(i).index(2)
        return self.player
