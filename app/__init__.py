from app.main import Escape

if __name__ == '__main__':
    game = Escape([[2, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]])
    game.show_car_park()
    game.find_player()
    print(game.start())
